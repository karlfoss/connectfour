# README #

### Connect Four ###

* Connect Four is a game where a player needs to get 4 chips lined up in a row in order to win. This can be done diagonally, horizontally, or vertically.
* Connect Four Background and Rules: https://en.wikipedia.org/wiki/Connect_Four

### How to run the project ###

* Clone the repository
* Set ConnectFour.Console as the startup project
* Use the command line interface to change game settings
* Start the game (Red player goes first)

### Contributor  ###

* Author: Karl Foss