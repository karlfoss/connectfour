﻿using ConnectFour.Core.Entities;
using ConnectFour.Core.Entities.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour.Console.Game
{
    public abstract class BaseGame
    {
        public Board GameBoard
        {
            get; set;
        }

        public List<BasePlayer> Players
        {
            get; set;
        }

        public BaseGame()
        {
            GameBoard = new Board();
        }

        public abstract void Start(List<BasePlayer> players);
    }
}
