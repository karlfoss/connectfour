﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using ConnectFour.Core;
using ConnectFour.Core.Entities;
using ConnectFour.Core.Entities.Game;

namespace ConnectFour.Console.Game
{
    public class Game : BaseGame
    {
        public Game() : base()
        {

        }

        public override void Start(List<BasePlayer> players)
        {
            //It's a two player game
            if (players.Count != 2)
            {
                throw new InvalidOperationException("Connect 4 is a two player game");
            }

            //Confirm that only one player of each color exists
            BasePlayer playerOne = players.Where(player => player.ChipColor == (int)Chip.Color.Red).Single();
            BasePlayer playerTwo = players.Where(player => player.ChipColor == (int)Chip.Color.Black).Single();
            playerOne.isTurn = true;
            playerTwo.isTurn = false;

            Players = players;

            Play();
        }

        private void Play()
        {
            bool isGameOver = false;
            do
            {
                BasePlayer currentPlayer = Players.Where(player => player.isTurn).Single();
                GameBoard.PrintGameStatus(currentPlayer.ChipColor);

                Stopwatch timeTakenToMakeMove = new Stopwatch();
                timeTakenToMakeMove.Start();

                Move nextMove = currentPlayer.ChooseMove((Board)GameBoard.Clone(), currentPlayer.ChipColor);
                GameBoard.MakeMove(nextMove);

                timeTakenToMakeMove.Stop();
                currentPlayer.TimeTakenToMove.Add(timeTakenToMakeMove.Elapsed);

                if (GameBoard.CheckForWin(currentPlayer.ChipColor))
                {
                    System.Console.Clear();
                    System.Console.WriteLine(GameBoard.ToString());
                    System.Console.WriteLine("Player " + currentPlayer.Name + " has won!");
                    System.Console.WriteLine("Press any key to continue...");
                    System.Console.ReadKey();
                    isGameOver = true;
                }
                else if (GameBoard.GameMoves.Count == GameConstants.MAXIUM_NUMBER_OF_GAME_MOVES)
                {
                    System.Console.Clear();
                    System.Console.WriteLine(GameBoard.ToString());
                    System.Console.WriteLine("No moves left, the game is a draw!");
                    System.Console.WriteLine("Press any key to continue...");
                    System.Console.ReadKey();
                    isGameOver = true;
                }
                else
                {
                    ChangeTurns();
                }

            } while (!isGameOver);
        }

        private void ChangeTurns()
        {
            foreach (BasePlayer player in Players)
            {
                player.isTurn = !player.isTurn;
            }
        }
    }
}
