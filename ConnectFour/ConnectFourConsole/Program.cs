﻿using System;
using System.Collections.Generic;
using ConnectFour.Core.Entities.Game;
using ConnectFour.Core.Entities;
using ConnectFour.Core.Entities.Player;
using ConnectFour.Console.Game;

namespace ConnectFourConsole
{
    class Program
    {
        private static string PlayerOneName = "You";
        private static string PlayerTwoName = "Terminator";
        private static int PlayerOneType = (int)PlayerType.Human;
        private static int PlayerTwoType = (int)PlayerType.HybridComputer;
        private static int PlayerOneChipColor = (int)Chip.Color.Red;
        private static int PlayerTwoChipColor = (int)Chip.Color.Black;

        private enum PlayerType
        {
            Human = 0,
            BruteForceComputer = 1,
            PositionComputer = 2,
            HybridComputer = 3
        }

        public static void Main(string[] args)
        {
            DisplayGameMenu();
        }

        private static void DisplayGameMenu()
        {
            bool exitGame = false;

            do
            {
                Console.Clear();
                Console.WriteLine("Player One Name: " + PlayerOneName);
                Console.WriteLine("Player One Color: " + String.Format(Enum.GetName(typeof(Chip.Color), PlayerOneChipColor)));
                Console.WriteLine("Player One Type: " + String.Format(Enum.GetName(typeof(PlayerType), PlayerOneType)));
                Console.WriteLine("Player Two Name: " + PlayerTwoName);
                Console.WriteLine("Player Two Color: " + String.Format(Enum.GetName(typeof(Chip.Color), PlayerTwoChipColor)));
                Console.WriteLine("Player Two Type: " + String.Format(Enum.GetName(typeof(PlayerType), PlayerTwoType)));
                Console.WriteLine("--------------------------");
                // Prompt the user for options
                Console.WriteLine("Options for Connect 4:");
                Console.WriteLine("\t1 - Play Game");
                Console.WriteLine("\t2 - Change Game Settings");
                Console.WriteLine("\t3 - Exit");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        List<BasePlayer> players = new List<BasePlayer>();
                        players.Add(InitializePlayer(PlayerOneName, PlayerOneType, PlayerOneChipColor));
                        players.Add(InitializePlayer(PlayerTwoName, PlayerTwoType, PlayerTwoChipColor));

                        BaseGame game = new Game();
                        game.Start(players);
                        break;
                    case "2":
                        // Add new logic flow after here
                        ChangeGameSettings();
                        break;
                    case "3":
                        exitGame = true;
                        break;
                }

            } while (!exitGame);

            Console.WriteLine("Thanks for playing!");
            System.Threading.Thread.Sleep(2000);
        }

        private static void ChangeGameSettings()
        {
            bool exitGameSettings = false;

            do
            {
                // Prompt the user for options
                Console.Clear();
                Console.WriteLine("Player One Name: " + PlayerOneName);
                Console.WriteLine("Player One Color: " + String.Format(Enum.GetName(typeof(Chip.Color), PlayerOneChipColor)));
                Console.WriteLine("Player One Type: " + String.Format(Enum.GetName(typeof(PlayerType), PlayerOneType)));
                Console.WriteLine("Player Two Name: " + PlayerTwoName);
                Console.WriteLine("Player Two Color: " + String.Format(Enum.GetName(typeof(Chip.Color), PlayerTwoChipColor)));
                Console.WriteLine("Player Two Type: " + String.Format(Enum.GetName(typeof(PlayerType), PlayerTwoType)));
                Console.WriteLine("--------------------------");
                Console.WriteLine("Update Connect Four Settings:");
                Console.WriteLine("\t1 - Change Player One Name");
                Console.WriteLine("\t2 - Change Player One Type");
                Console.WriteLine("\t3 - Change Player Two Name");
                Console.WriteLine("\t4 - Change Player Two Type");
                Console.WriteLine("\t5 - Change Player Colors");
                Console.WriteLine("\t6 - Exit Settings Changes");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        PlayerOneName = GetStringInput("Player One Name");
                        break;
                    case "2":
                        PlayerOneType = GetPlayerTypeInput("Player One Type");
                        break;
                    case "3":
                        PlayerTwoName = GetStringInput("Player Two Name");
                        break;
                    case "4":
                        PlayerTwoType = GetPlayerTypeInput("Player Two Type");
                        break;
                    case "5":
                        // Reverse the player colorss
                        if (PlayerOneChipColor == (int)Chip.Color.Red)
                        {
                            PlayerOneChipColor = (int)Chip.Color.Black;
                            PlayerTwoChipColor = (int)Chip.Color.Red;
                        }
                        else
                        {
                            PlayerOneChipColor = (int)Chip.Color.Red;
                            PlayerTwoChipColor = (int)Chip.Color.Black;
                        }
                        break;
                    case "6":
                        exitGameSettings = true;
                        break;
                }

            } while (!exitGameSettings);
        }

        private static string GetStringInput(string option)
        {
            Console.WriteLine("Please Enter the " + option);
            string optionChange = Console.ReadLine();

            while (string.IsNullOrEmpty(optionChange))
            {
                Console.WriteLine("Please enter a valid " + option);
                optionChange = Console.ReadLine();
            }

            return optionChange;
        }

        private static int GetPlayerTypeInput(string option)
        {
            Console.WriteLine("Please Enter the " + option);
            bool isValidPlayerTypeChosen = false;
            int playerTypeChosen = -1;

            do
            {
                Console.Clear();
                Console.WriteLine("Please Enter the " + option);

                // Prompt the user for options
                Console.WriteLine("Options for Computer Difficulty");
                Console.WriteLine("\t1 - Human - A human player controls the game chips with letters A-G");
                Console.WriteLine("\t2 - Brute Force Computer - A computer picks best path based on win/loss outcomes");
                Console.WriteLine("\t3 - Position Computer - A computer picks positions based on the number of favorable outcomes");
                Console.WriteLine("\t4 - Hybrid Computer - A computer picks moves by both brute force win/loss and position strength");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        playerTypeChosen = (int)PlayerType.Human;
                        isValidPlayerTypeChosen = true;
                        break;
                    case "2":
                        playerTypeChosen = (int)PlayerType.BruteForceComputer;
                        isValidPlayerTypeChosen = true;
                        break;
                    case "3":
                        playerTypeChosen = (int)PlayerType.PositionComputer;
                        isValidPlayerTypeChosen = true;
                        break;
                    case "4":
                        playerTypeChosen = (int)PlayerType.HybridComputer;
                        isValidPlayerTypeChosen = true;
                        break;
                }

            } while (!isValidPlayerTypeChosen);

            return playerTypeChosen;
        }

        private static BasePlayer InitializePlayer(string playerName, int playerType, int chipColor)
        {
            BasePlayer newPlayer;

            if (playerType == (int)PlayerType.Human)
            {
                newPlayer = new HumanPlayer { Name = playerName, ChipColor = chipColor };
            }
            else if (playerType == (int)PlayerType.PositionComputer)
            {
                newPlayer = new PositionComputerPlayer { Name = playerName, ChipColor = chipColor };
            }
            else if (playerType == (int)PlayerType.BruteForceComputer)
            {
                newPlayer = new BruteForceComputerPlayer { Name = playerName, ChipColor = chipColor };
            }
            else if (playerType == (int)PlayerType.HybridComputer)
            {
                newPlayer = new HybridComputerPlayer { Name = playerName, ChipColor = chipColor };
            }
            else
            {
                throw new NotImplementedException("The player type for " + playerName + " does not exist");
            }

            return newPlayer;
        }
    }
}
