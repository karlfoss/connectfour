﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour.Core
{
    public class GameConstants
    {
        public const int EMPTY = 0;
        public const int RED_CHIP = 1;
        public const int BLACK_CHIP = 2;

        public const int INVALID_MOVE = -1;

        public const int COLUMN = 0;
        public const int ROW = 1;
        public const int FIRST_COLUMN = 0;
        public const int SECOND_COLUMN = 1;
        public const int THRID_COLUMN = 2;
        public const int FOUTH_COLUMN = 3;
        public const int FIFTH_COLUMN = 4;
        public const int SIXTH_COLUMN = 5;
        public const int SEVENTH_COLUMN = 6;

        public const int MINIMUM_NUMBER_OF_MOVES_NEEDED_FOR_WIN = 7;
        public const int MAXIUM_NUMBER_OF_GAME_MOVES = 42;
    }
}
