﻿using System;
using System.Collections.Generic;
using System.Text;
using ConnectFour.Core.DataStructures;

namespace ConnectFour.Core.Entities.Game
{
    public class TreeNodeMove
    {
        public bool IsRoot
        {
            get; set;
        }

        public TreeNodeMove ParentMoveNode
        {
            get; set;
        }

        public Move MoveNode
        {
            get; set;
        }

        public Board GameBoard
        {
            get; set;
        }

        public IList<TreeNodeMove> ChildrenMoveNodes
        {
            get; set;
        }

        public TreeNodeMove(Move moveNode, bool isRootNode)
        {
            IsRoot = isRootNode;
            MoveNode = moveNode;
            ParentMoveNode = null;
            ChildrenMoveNodes = new List<TreeNodeMove>();
        }
    }
}
