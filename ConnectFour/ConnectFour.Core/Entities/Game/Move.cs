﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ConnectFour.Core.Entities.Game
{
    public class Move : ICloneable
    {
        public int ChipColor
        {
            get; set;
        }

        //X,Y coordinate of the player's move
        public int[] PlayerMove
        {
            get; set;
        }

        //Used for calculating the Minimax score of a possible move
        public MoveScore MoveScore
        {
            get; set;
        }

        public Object Clone()
        {
            Move cloneMove = new Move();
            cloneMove.ChipColor = ChipColor;
            cloneMove.MoveScore = (MoveScore)MoveScore.Clone();
            cloneMove.PlayerMove = new int[] { PlayerMove[GameConstants.COLUMN], PlayerMove[GameConstants.ROW] };

            return cloneMove;
        }

        public Move()
        {
            MoveScore = new MoveScore();
        }

        public Move(int chipColor)
        {
            ChipColor = chipColor;
            MoveScore = new MoveScore();
        }

        public Move(int[] playerMove, int chipColor)
        {
            ChipColor = chipColor;
            PlayerMove = playerMove;
            MoveScore = new MoveScore();
        }

        public override string ToString()
        {
            return "Move BruteForce Score: " + MoveScore.WinLoseScore + " Move Position Score: " + MoveScore.PositionScore + " PositionX: " + PlayerMove[GameConstants.COLUMN] + " PositionY: " +
                PlayerMove[GameConstants.ROW] + " Chip Color: " + String.Format(Enum.GetName(typeof(Chip.Color), ChipColor));
        }
    }
}
