﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour.Core.Entities.Game
{
    public class MoveScore : ICloneable
    {
        public int WinLoseScore
        {
            get; set;
        }

        public double PositionScore
        {
            get; set;
        }

        public Object Clone()
        {
            MoveScore cloneMoveScore = new MoveScore();
            int winLoseScore = WinLoseScore;
            double positionScore = PositionScore;

            return cloneMoveScore;
        }
    }
}
