﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using ConnectFour.Core;

namespace ConnectFour.Core.Entities.Game
{
    public class Board : ICloneable
    {
        private const int NUMBER_OF_ROWS = 6;
        private const int NUMBER_OF_COLUMNS = 7;

        private const bool NO_WINNER_FOUND = false;
        private const bool WINNER_FOUND = true;

        public int[,] AllHoles
        {
            get; set;
        }

        public IList<Move> GameMoves
        {
            get; set;
        }

        public Board()
        {
            AllHoles = new int[NUMBER_OF_COLUMNS, NUMBER_OF_ROWS];
            GameMoves = new List<Move>();
        }

        public Object Clone()
        {
            Board cloneBoard = new Board();

            for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS; columnIndex++)
            {
                for (int rowIndex = 0; rowIndex < NUMBER_OF_ROWS; rowIndex++)
                {
                    cloneBoard.AllHoles[columnIndex, rowIndex] = this.AllHoles[columnIndex, rowIndex];
                }
            }

            foreach (Move move in GameMoves)
            {
                Move cloneMove = (Move) move.Clone();
                cloneBoard.GameMoves.Add(cloneMove);
            }

            return cloneBoard;
        }

        public PossibleMoves FindAllMoves(int chipColor)
        {
            PossibleMoves possibleMoves = new PossibleMoves();
            for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS; columnIndex++)
            {
                possibleMoves.AllPossibleMoves[columnIndex] = CheckColumnForMove(columnIndex);
            }

            return possibleMoves;
        }

        public void MakeMove(Move move)
        {
            GameMoves.Add(move);
            AllHoles[move.PlayerMove[GameConstants.COLUMN], move.PlayerMove[GameConstants.ROW]] = move.ChipColor;
        }

        public bool CheckForWin(int chipColor)
        {
            if (GameMoves.Count < GameConstants.MINIMUM_NUMBER_OF_MOVES_NEEDED_FOR_WIN)
            {
                return NO_WINNER_FOUND;
            }

            if (CheckForHorizontalWin(chipColor) || CheckForVerticalWin(chipColor) || 
                CheckForDownwardDiagonalWin(chipColor) || CheckForUpwardDiagonalWin(chipColor))
            {
                return WINNER_FOUND;
            }

            return NO_WINNER_FOUND;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = NUMBER_OF_ROWS -1; i >= 0; i--)
            {
                stringBuilder.AppendFormat("| {0} {1} {2} {3} {4} {5} {6} |\n", 
                    AllHoles[0, i], AllHoles[1, i], AllHoles[2, i], AllHoles[3, i], 
                    AllHoles[4, i], AllHoles[5, i], AllHoles[6, i]);
            }
            stringBuilder.Append("| _ _ _ _ _ _ _ |");

            return stringBuilder.ToString();
        }

        public void PrintGameStatus(int chipColor)
        {
            Console.Clear();
            Console.WriteLine(String.Format(Enum.GetName(typeof(Chip.Color), chipColor) + " Player's Turn"));
            Console.WriteLine(ToString());
        }

        private int CheckColumnForMove(int columnNumber)
        {
            for (int rowIndex = 0; rowIndex < NUMBER_OF_ROWS; rowIndex++)
            {
                if (AllHoles[columnNumber, rowIndex] == GameConstants.EMPTY)
                {
                    return rowIndex;
                }
            }

            return GameConstants.INVALID_MOVE;
        }

        private bool CheckForHorizontalWin(int chipColor)
        {
            for (int rowIndex = 0; rowIndex < NUMBER_OF_ROWS; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS - 3; columnIndex++)
                {
                    if (AllHoles[columnIndex, rowIndex] == chipColor && AllHoles[columnIndex + 1, rowIndex] == chipColor &&
                        AllHoles[columnIndex + 2, rowIndex] == chipColor && AllHoles[columnIndex + 3, rowIndex] == chipColor)
                    {
                        return WINNER_FOUND;
                    }
                }
            }

            return NO_WINNER_FOUND;
        }

        private bool CheckForUpwardDiagonalWin(int chipColor)
        {
            for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS - 3; columnIndex++)
            {
                for (int rowIndex = 0; rowIndex < NUMBER_OF_ROWS - 3; rowIndex++)
                {
                    if (AllHoles[columnIndex, rowIndex] == chipColor && AllHoles[columnIndex + 1, rowIndex + 1] == chipColor &&
                        AllHoles[columnIndex + 2, rowIndex + 2] == chipColor && AllHoles[columnIndex + 3, rowIndex + 3] == chipColor)
                    {
                        return WINNER_FOUND;
                    }
                }
            }

            return NO_WINNER_FOUND;
        }

        private bool CheckForDownwardDiagonalWin(int chipColor)
        {
            for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS - 3; columnIndex++)
            {
                for (int rowIndex = NUMBER_OF_ROWS - 1; rowIndex > NUMBER_OF_ROWS - 4; rowIndex--)
                {
                    if (AllHoles[columnIndex, rowIndex] == chipColor && AllHoles[columnIndex + 1, rowIndex - 1] == chipColor &&
                        AllHoles[columnIndex + 2, rowIndex - 2] == chipColor && AllHoles[columnIndex + 3, rowIndex - 3] == chipColor)
                    {
                        return WINNER_FOUND;
                    }
                }
            }

            return NO_WINNER_FOUND;
        }

        private bool CheckForVerticalWin(int chipColor)
        {
            for (int columnIndex = 0; columnIndex < NUMBER_OF_COLUMNS; columnIndex++)
            {
                for (int rowIndex = 0; rowIndex < NUMBER_OF_ROWS - 3; rowIndex++)
                {
                    if (AllHoles[columnIndex, rowIndex] == chipColor && AllHoles[columnIndex, rowIndex + 1] == chipColor &&
                        AllHoles[columnIndex, rowIndex + 2] == chipColor && AllHoles[columnIndex, rowIndex + 3] == chipColor)
                    {
                        return WINNER_FOUND;
                    }
                }
            }

            return NO_WINNER_FOUND;
        }
    }
}
