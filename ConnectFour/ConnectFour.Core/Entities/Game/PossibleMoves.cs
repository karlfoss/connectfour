﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using ConnectFour.Core;
using ConnectFour.Core.Entities.Game;

namespace ConnectFour.Core.Entities.Game
{
    public class PossibleMoves
    {
        private const bool NO_VALID_MOVES = false;
        private const bool VALID_MOVE_FOUND = true;

        public int[] AllPossibleMoves
        {
            get; set;
        }

        public PossibleMoves()
        {
            AllPossibleMoves = new int[7];
            for (int i = 0; i < AllPossibleMoves.Length; i++)
            {
                AllPossibleMoves[i] = GameConstants.INVALID_MOVE;
            }
        }

        public bool IsThereAValidMove()
        {
            if (AllPossibleMoves[GameConstants.FIRST_COLUMN] == GameConstants.INVALID_MOVE &&
                    AllPossibleMoves[GameConstants.SECOND_COLUMN] == GameConstants.INVALID_MOVE &&
                    AllPossibleMoves[GameConstants.THRID_COLUMN] == GameConstants.INVALID_MOVE &&
                    AllPossibleMoves[GameConstants.FOUTH_COLUMN] == GameConstants.INVALID_MOVE &&
                    AllPossibleMoves[GameConstants.FIFTH_COLUMN] == GameConstants.INVALID_MOVE &&
                    AllPossibleMoves[GameConstants.SIXTH_COLUMN] == GameConstants.INVALID_MOVE &&
                    AllPossibleMoves[GameConstants.SEVENTH_COLUMN] == GameConstants.INVALID_MOVE)
            {
                return NO_VALID_MOVES;
            }

            return VALID_MOVE_FOUND;
        }
    }
}
