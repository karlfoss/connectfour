﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour.Core.Entities.Game
{
    public class Chip
    {
        public enum Color
        {
            None = GameConstants.EMPTY,
            Red = GameConstants.RED_CHIP,
            Black = GameConstants.BLACK_CHIP
        }
    }
}
