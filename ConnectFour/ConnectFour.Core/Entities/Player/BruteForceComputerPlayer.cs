﻿using ConnectFour.Core.Entities.Game;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConnectFour.Core.Entities.Player
{
    /**
     * This is a true MiniMax implementation. It will only consider events that lead
     * to a definite outcome. However, it lacks Heuristics that setup for stronger positions 
     * later in the game. IE, if there is no winning/losing position, it chooses randomly
     */
    public class BruteForceComputerPlayer : BasePlayer
    {
        //Debug only
        //private TreeNodeMove RootTreeNodeMove;

        /**
         * AI Enhancements:
         *  Reduce computation burden to allow greater depth search
         *  Increase Depth Search as game goes along because outcomes are simpler
         *  Add Heuristics that encourages the bot to focus on the center columns
         */
        public BruteForceComputerPlayer() : base()
        {

        }

        public override Move ChooseMove(Board gameBoard, int chipColor)
        {
            bool isCurrentPlayer = true;
            return MiniMax(gameBoard, chipColor, isCurrentPlayer);
        }

        private Move MiniMax(Board gameBoard, int chipColor, bool isCurrentPlayer)
        {
            //Debug only
            //Move rootTreeMoveNode = new Move(42, (int)Chip.Color.None);
            //RootTreeNodeMove = new TreeNodeMove(rootTreeMoveNode, true);
            //RootTreeNodeMove.GameBoard = gameBoard;

            MoveScore moveScore = new MoveScore();
            
            int depth = STARTING_DEPTH;
            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            List<Move> movesToConsider = new List<Move>();

            for (int columnIndex = 0; columnIndex < possibleMoves.AllPossibleMoves.Length; columnIndex++)
            {
                if (possibleMoves.AllPossibleMoves[columnIndex] != GameConstants.INVALID_MOVE)
                {
                    Move possibleMove = new Move(chipColor);
                    possibleMove.PlayerMove = new int[] { columnIndex, possibleMoves.AllPossibleMoves[columnIndex] };
                    Board updatedGameBoard = (Board)gameBoard.Clone();
                    updatedGameBoard.MakeMove(possibleMove);

                    //Debug Only
                    //TreeNodeMove newChildTreeNodeMove = new TreeNodeMove(possibleMove, false);
                    //RootTreeNodeMove.ChildrenMoveNodes.Add(newChildTreeNodeMove);
                    //newChildTreeNodeMove.ParentMoveNode = RootTreeNodeMove;
                    //newChildTreeNodeMove.GameBoard = updatedGameBoard;

                    if (updatedGameBoard.CheckForWin(chipColor))
                    {
                        return possibleMove;
                    }

                    if (chipColor == (int)Chip.Color.Black)
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Red, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }
                    else
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Black, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }

                    movesToConsider.Add(possibleMove);
                }
            }

            moveScore.WinLoseScore = movesToConsider.Max(move => move.MoveScore.WinLoseScore);
            IList<Move> allMovesWithMaxScore = movesToConsider.Where(move => move.MoveScore.WinLoseScore == moveScore.WinLoseScore).ToList();
            Random rn = new Random();
            return allMovesWithMaxScore[rn.Next(0, allMovesWithMaxScore.Count)];
        }

        private MoveScore MiniMax(Board gameBoard, int depth, int chipColor, bool isCurrentPlayer/**, TreeNodeMove treeNodeMove**/)
        {
            MoveScore moveScore = new MoveScore();

            //Check if an end condition is met
            if (gameBoard.GameMoves.Count == GameConstants.MAXIUM_NUMBER_OF_GAME_MOVES)
            {
                moveScore.WinLoseScore = DRAWN_GAME;
                return moveScore;
            }

            //NOTE: There are up to 4,531,985,219,092 positions in Connect Four. So the bot can't check everything by ending the depth search.
            if (depth > MAX_DEPTH_TO_SEARCH_DEFAULT)
            {
                moveScore.WinLoseScore = NO_WINNER_YET;
                return moveScore;
            }

            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            List<Move> movesToConsider = new List<Move>();

            for (int columnIndex = 0; columnIndex < possibleMoves.AllPossibleMoves.Length; columnIndex++)
            {
                if (possibleMoves.AllPossibleMoves[columnIndex] != GameConstants.INVALID_MOVE)
                {
                    Move possibleMove = new Move(chipColor);
                    possibleMove.PlayerMove = new int[] { columnIndex, possibleMoves.AllPossibleMoves[columnIndex] };
                    Board updatedGameBoard = (Board)gameBoard.Clone();
                    updatedGameBoard.MakeMove(possibleMove);

                    //Debug Only
                    //TreeNodeMove newChildTreeNodeMove = new TreeNodeMove(possibleMove, false);
                    //treeNodeMove.ChildrenMoveNodes.Add(newChildTreeNodeMove);
                    //newChildTreeNodeMove.ParentMoveNode = treeNodeMove;
                    //newChildTreeNodeMove.GameBoard = updatedGameBoard;

                    if (updatedGameBoard.CheckForWin(chipColor))
                    {
                        if (isCurrentPlayer)
                        {
                            moveScore.WinLoseScore = WON_GAME;
                        }
                        else
                        {
                            moveScore.WinLoseScore = LOST_GAME;
                        }
                        return moveScore;
                    }
                    else if (chipColor == (int)Chip.Color.Black)
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Red, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }
                    else
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Black, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }

                    movesToConsider.Add(possibleMove);
                }
            }

            if (isCurrentPlayer)
            {
                moveScore.WinLoseScore = movesToConsider.Max(move => move.MoveScore.WinLoseScore);
            }
            else
            {
                moveScore.WinLoseScore = movesToConsider.Min(move => move.MoveScore.WinLoseScore);
            }

            return moveScore;
        }
    }
}
