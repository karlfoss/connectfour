﻿using ConnectFour.Core.Entities.Game;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ConnectFour.Core.Entities
{
    public abstract class BasePlayer
    {
        protected const int STARTING_DEPTH = 0;
        protected const int DRAWN_GAME = 0;
        protected const int NO_WINNER_YET = 0;
        protected const int WON_GAME = 10;
        protected const int LOST_GAME = -10;

        protected const int MAX_DEPTH_TO_SEARCH_DEFAULT = 7;

        public int ChipColor
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public bool isTurn
        {
            get; set;
        }

        public IList<TimeSpan> TimeTakenToMove
        {
            get; set;
        }

        public BasePlayer()
        {
            TimeTakenToMove = new List<TimeSpan>();
        }

        public abstract Move ChooseMove(Board gameBoard, int chipColor);
    }
}
