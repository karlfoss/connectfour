﻿using ConnectFour.Core.Entities.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using ConnectFour.Core.DataStructures;

namespace ConnectFour.Core.Entities.Player
{
    /**
     *  This AI attempts to find a balance between choosing guaranteed wins and also choosing
     *  better positions when no immediate win/loss is there to worry about
     */
    public class HybridComputerPlayer : BasePlayer
    {
        //Debug only
        //private TreeNodeMove RootTreeNodeMove;

        /**
         * AI Enhancements:
         *  Improve heuristic algorithm to focus on position advantage vs quick (but easily stopped wins)
         */

        public int MAX_DEPTH_TO_SEARCH = 7;

        public HybridComputerPlayer() : base()
        {

        }

        public override Move ChooseMove(Board gameBoard, int chipColor)
        {
            bool isCurrentPlayer = true;
            if (gameBoard.GameMoves.Count < 3)
            {
                
            }
            else if (gameBoard.GameMoves.Count < 6)
            {
                MAX_DEPTH_TO_SEARCH = 8;
            }
            else if (gameBoard.GameMoves.Count < 8)
            {
                MAX_DEPTH_TO_SEARCH = 9;
            }
            else if (gameBoard.GameMoves.Count < 10)
            {
                MAX_DEPTH_TO_SEARCH = 10;
            }
            else
            {
                MAX_DEPTH_TO_SEARCH = 20;
            }

            return MiniMax(gameBoard, chipColor, isCurrentPlayer);
        }

        private Move MiniMax(Board gameBoard, int chipColor, bool isCurrentPlayer)
        {
            MoveScore moveScore = new MoveScore();

            //Debug only
            //Move rootTreeMoveNode = new Move(42, (int)Chip.Color.None);
            //RootTreeNodeMove = new TreeNodeMove(rootTreeMoveNode, true);
            //RootTreeNodeMove.GameBoard = gameBoard;
            
            int depth = STARTING_DEPTH;
            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            List<Move> movesToConsider = new List<Move>();

            for (int columnIndex = 0; columnIndex < possibleMoves.AllPossibleMoves.Length; columnIndex++)
            {
                if (possibleMoves.AllPossibleMoves[columnIndex] != GameConstants.INVALID_MOVE)
                {
                    Move possibleMove = new Move(chipColor);
                    possibleMove.PlayerMove = new int[] { columnIndex, possibleMoves.AllPossibleMoves[columnIndex] };
                    Board updatedGameBoard = (Board)gameBoard.Clone();
                    updatedGameBoard.MakeMove(possibleMove);

                    //Debug Only
                    //TreeNodeMove newChildTreeNodeMove = new TreeNodeMove(possibleMove, false);
                    //RootTreeNodeMove.ChildrenMoveNodes.Add(newChildTreeNodeMove);
                    //newChildTreeNodeMove.ParentMoveNode = RootTreeNodeMove;
                    //newChildTreeNodeMove.GameBoard = updatedGameBoard;

                    if (updatedGameBoard.CheckForWin(chipColor))
                    {
                        return possibleMove;
                    }

                    if (chipColor == (int)Chip.Color.Black)
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Red, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }
                    else
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Black, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }

                    movesToConsider.Add(possibleMove);
                }
            }

            //Consider the brute force score, then use position score to determine the next move, then random if there is still a tie
            moveScore.WinLoseScore = movesToConsider.Max(move => move.MoveScore.WinLoseScore);
            IList<Move> allMovesWithMaxBruteForceScore = movesToConsider.Where(move => move.MoveScore.WinLoseScore == moveScore.WinLoseScore).ToList();
            moveScore.PositionScore = allMovesWithMaxBruteForceScore.Max(move => move.MoveScore.PositionScore);
            IList<Move> allMovesWithMaxHybridScore = allMovesWithMaxBruteForceScore.Where(move => move.MoveScore.PositionScore == moveScore.PositionScore).ToList();
            Random rn = new Random();
            return allMovesWithMaxHybridScore[rn.Next(0, allMovesWithMaxHybridScore.Count)];
        }

        private MoveScore MiniMax(Board gameBoard, int depth, int chipColor, bool isCurrentPlayer/**, TreeNodeMove treeNodeMove**/)
        {
            MoveScore moveScore = new MoveScore();

            //Check if an end condition is met
            if (gameBoard.GameMoves.Count == GameConstants.MAXIUM_NUMBER_OF_GAME_MOVES)
            {
                moveScore.PositionScore = DRAWN_GAME;
                moveScore.WinLoseScore = DRAWN_GAME;
                return moveScore;
            }

            //NOTE: depth can reach up to 4,531,985,219,092 positions in Connect Four. So the bot can't check everything
            if (depth > 7)
            {
                moveScore.PositionScore = NO_WINNER_YET;
                moveScore.WinLoseScore = NO_WINNER_YET;
                return moveScore;
            }

            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            List<Move> movesToConsider = new List<Move>();

            for (int columnIndex = 0; columnIndex < possibleMoves.AllPossibleMoves.Length; columnIndex++)
            {
                if (possibleMoves.AllPossibleMoves[columnIndex] != GameConstants.INVALID_MOVE)
                {
                    Move possibleMove = new Move(chipColor);
                    possibleMove.PlayerMove = new int[] { columnIndex, possibleMoves.AllPossibleMoves[columnIndex] };
                    Board updatedGameBoard = (Board)gameBoard.Clone();
                    updatedGameBoard.MakeMove(possibleMove);

                    //Debug Only
                    //TreeNodeMove newChildTreeNodeMove = new TreeNodeMove(possibleMove, false);
                    //treeNodeMove.ChildrenMoveNodes.Add(newChildTreeNodeMove);
                    //newChildTreeNodeMove.ParentMoveNode = treeNodeMove;
                    //newChildTreeNodeMove.GameBoard = updatedGameBoard;

                    if (updatedGameBoard.CheckForWin(chipColor))
                    {
                        if (isCurrentPlayer)
                        {
                            moveScore.PositionScore = WON_GAME;
                            moveScore.WinLoseScore = WON_GAME;
                            return moveScore;
                        }
                        else
                        {
                            moveScore.PositionScore = LOST_GAME;
                            moveScore.WinLoseScore = LOST_GAME;
                            return moveScore;
                        }
                    }
                    else if (chipColor == (int)Chip.Color.Black)
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Red, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }
                    else
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Black, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }

                    movesToConsider.Add(possibleMove);
                }
            }

            moveScore.PositionScore = movesToConsider.Average(move => move.MoveScore.PositionScore);
            if (isCurrentPlayer)
            {
                moveScore.WinLoseScore = movesToConsider.Max(move => move.MoveScore.WinLoseScore);
            }
            else
            {
                moveScore.WinLoseScore = movesToConsider.Min(move => move.MoveScore.WinLoseScore);
            }

            return moveScore;
        }
    }
}
