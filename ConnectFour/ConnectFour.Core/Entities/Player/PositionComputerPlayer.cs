﻿using ConnectFour.Core.Entities.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using ConnectFour.Core.DataStructures;

namespace ConnectFour.Core.Entities.Player
{
    /**
     *  This AI naturally picks better positions, but overestimates positive outcomes
     *  As a result, it does a better job of focusing on the middle which wins in the long term,
     *  but will then stray from the path by wasting 3 in a rows and allowing the opponent to block
     *  the attack. Rather than waiting until a guaranteed win
     */
    public class PositionComputerPlayer : BasePlayer
    {
        //Debug only
        //private TreeNodeMove RootTreeNodeMove;

        /**
         * AI Enhancements:
         *  Improve heuristic algorithm to focus on position advantage vs quick (but easily stopped wins)
         */

        public PositionComputerPlayer() : base()
        {

        }

        public override Move ChooseMove(Board gameBoard, int chipColor)
        {
            bool isCurrentPlayer = true;
            return MiniMax(gameBoard, chipColor, isCurrentPlayer);
        }

        private Move MiniMax(Board gameBoard, int chipColor, bool isCurrentPlayer)
        {
            MoveScore moveScore = new MoveScore();

            //Debug only
            //Move rootTreeMoveNode = new Move(42, (int)Chip.Color.None);
            //RootTreeNodeMove = new TreeNodeMove(rootTreeMoveNode, true);
            //RootTreeNodeMove.GameBoard = gameBoard;
            
            int depth = STARTING_DEPTH;
            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            List<Move> movesToConsider = new List<Move>();

            for (int columnIndex = 0; columnIndex < possibleMoves.AllPossibleMoves.Length; columnIndex++)
            {
                if (possibleMoves.AllPossibleMoves[columnIndex] != GameConstants.INVALID_MOVE)
                {
                    Move possibleMove = new Move(chipColor);
                    possibleMove.PlayerMove = new int[] { columnIndex, possibleMoves.AllPossibleMoves[columnIndex] };
                    Board updatedGameBoard = (Board)gameBoard.Clone();
                    updatedGameBoard.MakeMove(possibleMove);

                    //Debug Only
                    //TreeNodeMove newChildTreeNodeMove = new TreeNodeMove(possibleMove, false);
                    //RootTreeNodeMove.ChildrenMoveNodes.Add(newChildTreeNodeMove);
                    //newChildTreeNodeMove.ParentMoveNode = RootTreeNodeMove;
                    //newChildTreeNodeMove.GameBoard = updatedGameBoard;

                    if (updatedGameBoard.CheckForWin(chipColor))
                    {
                        return possibleMove;
                    }

                    if (chipColor == (int)Chip.Color.Black)
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Red, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }
                    else
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Black, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }

                    movesToConsider.Add(possibleMove);
                }
            }

            moveScore.PositionScore = movesToConsider.Max(move => move.MoveScore.PositionScore);
            IList<Move> allMovesWithMaxScore = movesToConsider.Where(move => move.MoveScore.PositionScore == moveScore.PositionScore).ToList();
            Random rn = new Random();
            return allMovesWithMaxScore[rn.Next(0, allMovesWithMaxScore.Count)];
        }

        private MoveScore MiniMax(Board gameBoard, int depth, int chipColor, bool isCurrentPlayer/**, TreeNodeMove treeNodeMove**/)
        {
            MoveScore moveScore = new MoveScore();

            //Check if an end condition is met
            if (gameBoard.GameMoves.Count == GameConstants.MAXIUM_NUMBER_OF_GAME_MOVES)
            {
                moveScore.PositionScore = DRAWN_GAME;
                return moveScore;
            }

            //NOTE: depth can reach up to 4,531,985,219,092 positions in Connect Four. So the bot can't check everything
            if (depth > MAX_DEPTH_TO_SEARCH_DEFAULT)
            {
                moveScore.PositionScore = NO_WINNER_YET;
                return moveScore;
            }

            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            List<Move> movesToConsider = new List<Move>();

            for (int columnIndex = 0; columnIndex < possibleMoves.AllPossibleMoves.Length; columnIndex++)
            {
                if (possibleMoves.AllPossibleMoves[columnIndex] != GameConstants.INVALID_MOVE)
                {
                    Move possibleMove = new Move(chipColor);
                    possibleMove.PlayerMove = new int[] { columnIndex, possibleMoves.AllPossibleMoves[columnIndex] };
                    Board updatedGameBoard = (Board)gameBoard.Clone();
                    updatedGameBoard.MakeMove(possibleMove);

                    //Debug Only
                    //TreeNodeMove newChildTreeNodeMove = new TreeNodeMove(possibleMove, false);
                    //treeNodeMove.ChildrenMoveNodes.Add(newChildTreeNodeMove);
                    //newChildTreeNodeMove.ParentMoveNode = treeNodeMove;
                    //newChildTreeNodeMove.GameBoard = updatedGameBoard;

                    if (updatedGameBoard.CheckForWin(chipColor))
                    {
                        if (isCurrentPlayer)
                        {
                            moveScore.PositionScore = WON_GAME;
                            return moveScore;
                        }
                        else
                        {
                            moveScore.PositionScore = LOST_GAME;
                            return moveScore;
                        }
                    }
                    else if (chipColor == (int)Chip.Color.Black)
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Red, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }
                    else
                    {
                        possibleMove.MoveScore = MiniMax(updatedGameBoard, depth + 1, (int)Chip.Color.Black, !isCurrentPlayer/**, newChildTreeNodeMove**/);
                    }

                    movesToConsider.Add(possibleMove);
                }
            }

            moveScore.PositionScore = movesToConsider.Average(move => move.MoveScore.PositionScore);

            return moveScore;
        }
    }
}
