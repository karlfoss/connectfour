﻿using ConnectFour.Core.Entities.Game;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConnectFour.Core.Entities
{
    public class HumanPlayer : BasePlayer
    {
        public HumanPlayer() : base()
        {

        }

        public override Move ChooseMove(Board gameBoard, int chipColor)
        {
            PossibleMoves possibleMoves = gameBoard.FindAllMoves(chipColor);
            bool moveSelected = false;
            Move newMove = new Move(chipColor);

            do
            {
                gameBoard.PrintGameStatus(chipColor);
                Console.WriteLine("  a b c d e f g  ");
                Console.WriteLine("Choose a move");
                switch (Console.ReadLine())
                {
                    case "a":
                        if (possibleMoves.AllPossibleMoves[GameConstants.FIRST_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.FIRST_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.FIRST_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                    case "b":
                        if (possibleMoves.AllPossibleMoves[GameConstants.SECOND_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.SECOND_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.SECOND_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                    case "c":
                        if (possibleMoves.AllPossibleMoves[GameConstants.THRID_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.THRID_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.THRID_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                    case "d":
                        if (possibleMoves.AllPossibleMoves[GameConstants.FOUTH_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.FOUTH_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.FOUTH_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                    case "e":
                        if (possibleMoves.AllPossibleMoves[GameConstants.FIFTH_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.FIFTH_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.FIFTH_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                    case "f":
                        if (possibleMoves.AllPossibleMoves[GameConstants.SIXTH_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.SIXTH_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.SIXTH_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                    case "g":
                        if (possibleMoves.AllPossibleMoves[GameConstants.SEVENTH_COLUMN] != GameConstants.INVALID_MOVE)
                        {
                            newMove.PlayerMove = new int[] { GameConstants.SEVENTH_COLUMN, possibleMoves.AllPossibleMoves[GameConstants.SEVENTH_COLUMN] };
                            moveSelected = true;
                        }
                        break;
                }

            } while (!moveSelected);

            return newMove;
        }
    }
}
