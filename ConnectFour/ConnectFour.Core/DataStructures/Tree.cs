﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour.Core.DataStructures
{
    //TODO: make a generic class for practice to use as the tree structure
    public class Tree<T>
    {
        private T _parentNode;
        private T _node;
        private IList<T> _childrenNodes;

        public Tree(T parentNode, T node)
        {
            _node = node;
            _parentNode = parentNode;
            _childrenNodes = new List<T>();
        }

        public T ParentNode
        {
            get { return _parentNode; }
            set { _parentNode = value; }
        }

        public T Node
        {
            get { return _node; }
            set { _node = value; }
        }

        public IList<T> ChildrenNodes
        {
            get { return _childrenNodes; }
            set { _childrenNodes = value; }
        }

        public void AddChildNode(T childNode)
        {
            _childrenNodes.Add(childNode);
        }
    }
}
